var express = require('express');
const { start } = require('repl');
var app = express();
var server = require('http').Server(app);
var io = require('socket.io').listen(server);
var players = {};
var star = {
    x: Math.floor(Math.random() * 700) + 50,
    y: Math.floor(Math.random() * 500) + 50
};
var scores = {
    blue: 0,
    red:0
}

app.use(express.static(__dirname + '/public'));

app.get ('/', function(req, res){
    res.sendFile(__dirname + '/index.html');
});

io.on('connection', function(socket) {
    console.log('a user connected');

    //create map and send it to map obj

    //create new player and add it to players obj
    players[socket.id] = {
        rotation: 0,
        x: Math.floor(Math.random() * 700) + 50,
        y: Math.floor(Math.random() * 500) + 50,
        playerId: socket.id,
        team: (Math.floor(Math.random() * 2) == 0) ? 'red' : 'blue'
    };
    //send players obj to new player
    socket.emit('currentPlayers', players);
    //update all other players of the new player
    socket.broadcast.emit('newPlayer', players[socket.id]);

    //send star object ot new player
    socket.emit ('starLocation', star);
    //send current scores
    socket.emit('scoreUpdate', scores);

    socket.on('disconnect', function (){
        console.log('user disconnected');

        //remove player from players obj
        delete players[socket.id];
        //emite message to all players to remove player
        io.emit('disconnect', socket.id);
    });

    //when player moves update player data
    socket.on('playerMovement', function(movementData){
        players[socket.id].x = movementData.x;
        players[socket.id].y = movementData.y;
        players[socket.id].rotation = movementData.rotation;
        //emit message to all players about player that moved
        socket.broadcast.emit('playerMoved', players[socket.id]);
    });

    socket.on('starCollected', function () {
        if (players[socket.id].team === 'red') {
            scores.red += 10;
        } else {
            scores.blue += 10;
        }
        star.x = Math.floor(Math.random() * 700) + 50;
        star.y = Math.floor(Math.random() * 500) + 50;
        io.emit('starLocation', star);
        io.emit('scoreUpdate', scores);
    });
});

server.listen(8081, function () {
    console.log(`Listening on ${server.address().port}`);
});

