class BootScene extends Phaser.Scene {
    constructor() {
      super('Boot');
    }
  
    preload() {
      // load images
      this.loadImages();
      // load spritesheets
      this.loadSpriteSheets();
      // load tilemap
      this.loadTileMap();
    }
  
    loadImages() {
      // load the map tileset image
      this.load.image('background', 'assets/maps/background.png');
      this.load.image('ship', 'assets/spaceShips_001.png');
      this.load.image('otherPlayer', 'assets/enemyBlack5.png');
      this.load.image('star', 'assets/star_gold.png');
      this.load.image('button1', 'assets/images/ui/blue_button01.png')
      this.load.image('button2', 'assets/images/ui/blue_button02.png')
    }
  
    loadSpriteSheets() {
      this.load.spritesheet('items', 'assets/images/items.png', { frameWidth: 32, frameHeight: 32 });
      this.load.spritesheet('characters', 'assets/images/characters.png', { frameWidth: 32, frameHeight: 32 });
      this.load.spritesheet('monsters', 'assets/images/monsters.png', { frameWidth: 32, frameHeight: 32 });
    }
  
    loadTileMap() {
      // map made with Tiled in JSON format
      this.load.tilemapTiledJSON('map', 'assets/maps/dngn.json');
    }
  
    create() {
      this.scene.start('Title');
    }
  }
  