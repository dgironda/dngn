class GameManager {
    constructor(scene, mapData){
        this.scene = scene;
        this.mapData = mapData;

        this.spawners = {};
        this.players = {};

        this.playerLocations = [];
    }
    
    setup(){
        this.parseMapData();
    }

    parseMapData() {
        this.mapData.forEach((layer) => {
          if (layer.name === 'player_locations') {
            layer.objects.forEach((obj) => {
              this.playerLocations.push([obj.x + (obj.width / 2), obj.y - (obj.height / 2)]);
            });
          }
        });
      }
}


