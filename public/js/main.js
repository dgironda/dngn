var config = {
    type: Phaser.AUTO,
    parent: 'phaser-example',
    width: 800,
    height: 800,
    physics: {
        default: 'arcade',
        arcade: {
            debug: false,
            gravity: {y:0}
        }
    },
    scene: [
        BootScene,
        TitleScene,
        GameScene,
    ]
};

var game = new Phaser.Game(config);

